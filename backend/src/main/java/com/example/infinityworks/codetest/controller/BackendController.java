package com.example.infinityworks.codetest.controller;

import com.example.infinityworks.codetest.domain.Rating;
import com.example.infinityworks.codetest.domain.Region;
import com.example.infinityworks.codetest.exceptions.RatingsException;
import com.example.infinityworks.codetest.functions.BackendFunctions;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController()
@RequestMapping("/api")
public class BackendController {

    private static final Logger LOG = LoggerFactory.getLogger(BackendController.class);

    private static final String BASE_URL = "http://api.ratings.food.gov.uk/";

    private RestTemplate restTemplate = new RestTemplate();

    private HttpHeaders headers = new HttpHeaders();

    private BackendFunctions functions = new BackendFunctions();

    @GetMapping(path="/regions")
    public @ResponseBody List<Region> getRegions() throws JSONException {
        LOG.info("Getting regions list");

        headers.set("x-api-version", "2");
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<String> response
                = restTemplate.exchange(BASE_URL + "/Authorities/basic", HttpMethod.GET, entity, String.class);
        return functions.parseRegions(response.getBody());
    }

    @GetMapping(path="/ratings/{id}")
    public @ResponseBody List<Rating> getRatingsUsingRegionId(@PathVariable("id") long id) throws RatingsException {
        LOG.info("Calculating percentages for a regions establishments");

        headers.set("x-api-version", "2");
        HttpEntity entity = new HttpEntity(headers);
        //get forbidden error without pageSize added
        ResponseEntity<String> response = restTemplate.exchange(BASE_URL +
                "/Establishments?LocalAuthorityId=" + id + "&pageSize=0", HttpMethod.GET, entity, String.class);
        return functions.getRatings(response.getBody());
    }

}
