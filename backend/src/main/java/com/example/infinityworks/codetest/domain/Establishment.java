package com.example.infinityworks.codetest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "FHRSID",
        "LocalAuthorityBusinessID",
        "BusinessName",
        "BusinessType",
        "BusinessTypeID",
        "AddressLine1",
        "AddressLine2",
        "AddressLine3",
        "AddressLine4",
        "PostCode",
        "Phone",
        "RatingValue",
        "RatingKey",
        "RatingDate",
        "LocalAuthorityCode",
        "LocalAuthorityName",
        "LocalAuthorityWebSite",
        "LocalAuthorityEmailAddress",
        "scores",
        "SchemeType",
        "geocode",
        "RightToReply",
        "Distance",
        "NewRatingPending",
        "meta",
        "links"
})
public class Establishment {

    @JsonProperty("FHRSID")
    public Integer fHRSID;
    @JsonProperty("LocalAuthorityBusinessID")
    public String localAuthorityBusinessID;
    @JsonProperty("BusinessName")
    public String businessName;
    @JsonProperty("BusinessType")
    public String businessType;
    @JsonProperty("BusinessTypeID")
    public Integer businessTypeID;
    @JsonProperty("AddressLine1")
    public String addressLine1;
    @JsonProperty("AddressLine2")
    public String addressLine2;
    @JsonProperty("AddressLine3")
    public String addressLine3;
    @JsonProperty("AddressLine4")
    public String addressLine4;
    @JsonProperty("PostCode")
    public String postCode;
    @JsonProperty("Phone")
    public String phone;
    @JsonProperty("RatingValue")
    public String ratingValue;
    @JsonProperty("RatingKey")
    public String ratingKey;
    @JsonProperty("RatingDate")
    public String ratingDate;
    @JsonProperty("LocalAuthorityCode")
    public String localAuthorityCode;
    @JsonProperty("LocalAuthorityName")
    public String localAuthorityName;
    @JsonProperty("LocalAuthorityWebSite")
    public String localAuthorityWebSite;
    @JsonProperty("LocalAuthorityEmailAddress")
    public String localAuthorityEmailAddress;
    @JsonProperty("scores")
    public Scores scores;
    @JsonProperty("SchemeType")
    public String schemeType;
    @JsonProperty("geocode")
    public Geocode geocode;
    @JsonProperty("RightToReply")
    public String rightToReply;
    @JsonProperty("Distance")
    public Object distance;
    @JsonProperty("NewRatingPending")
    public Boolean newRatingPending;
    @JsonProperty("meta")
    public Meta meta;
    @JsonProperty("links")
    public List<Object> links = null;
}
