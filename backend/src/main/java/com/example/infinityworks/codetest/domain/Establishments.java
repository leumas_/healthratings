package com.example.infinityworks.codetest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "establishments",
        "meta",
        "links"
})
public class Establishments {
    @JsonProperty("establishments")
    public List<Establishment> establishments = null;
    @JsonProperty("meta")
    public Meta meta;
    @JsonProperty("links")
    public List<Object> links = null;
}
