package com.example.infinityworks.codetest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "longitude",
        "latitude"
})
public class Geocode {

    @JsonProperty("longitude")
    public String longitude;
    @JsonProperty("latitude")
    public String latitude;

}
