package com.example.infinityworks.codetest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "dataSource",
        "extractDate",
        "itemCount",
        "returncode",
        "totalCount",
        "totalPages",
        "pageSize",
        "pageNumber"
})
public class Meta {

    @JsonProperty("dataSource")
    public Object dataSource;
    @JsonProperty("extractDate")
    public String extractDate;
    @JsonProperty("itemCount")
    public Integer itemCount;
    @JsonProperty("returncode")
    public Object returncode;
    @JsonProperty("totalCount")
    public Integer totalCount;
    @JsonProperty("totalPages")
    public Integer totalPages;
    @JsonProperty("pageSize")
    public Integer pageSize;
    @JsonProperty("pageNumber")
    public Integer pageNumber;

}
