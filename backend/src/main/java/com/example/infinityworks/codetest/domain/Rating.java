package com.example.infinityworks.codetest.domain;

public class Rating {
    public String starValue;
    public String percentage;

    public Rating(String starValue, String percentage) {
        this.starValue = starValue;
        this.percentage = percentage;
    }
}
