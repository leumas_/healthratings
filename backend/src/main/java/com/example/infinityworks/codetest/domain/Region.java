package com.example.infinityworks.codetest.domain;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

public class Region {
    @Getter
    @Setter
    @NotNull
    private String id;

    @Getter
    @Setter
    @NotNull
    private String regionName;

    public Region(String id, String regionName) {
        this.id = id;
        this.regionName = regionName;
    }
}
