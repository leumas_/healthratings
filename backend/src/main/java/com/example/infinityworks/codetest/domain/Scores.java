package com.example.infinityworks.codetest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Hygiene",
        "Structural",
        "ConfidenceInManagement"
})
public class Scores {

    @JsonProperty("Hygiene")
    public Integer hygiene;
    @JsonProperty("Structural")
    public Integer structural;
    @JsonProperty("ConfidenceInManagement")
    public Integer confidenceInManagement;

}
