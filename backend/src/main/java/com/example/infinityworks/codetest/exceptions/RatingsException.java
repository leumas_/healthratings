package com.example.infinityworks.codetest.exceptions;

public class RatingsException extends Exception
{
    // Parameterless Constructor
    public RatingsException() {}

    // Constructor that accepts a message
    public RatingsException(String message)
    {
        super(message);
    }
}