package com.example.infinityworks.codetest.functions;

import com.example.infinityworks.codetest.controller.BackendController;
import com.example.infinityworks.codetest.domain.Establishment;
import com.example.infinityworks.codetest.domain.Establishments;
import com.example.infinityworks.codetest.domain.Rating;
import com.example.infinityworks.codetest.domain.Region;
import com.example.infinityworks.codetest.exceptions.RatingsException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class BackendFunctions {

    private static final Logger LOG = LoggerFactory.getLogger(BackendController.class);

    public List<Region> parseRegions(String regionsUnparsed) throws JSONException {
        LOG.info("Parsing regions");
        List<Region> regions = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(regionsUnparsed);
            //loop over json array authorities
            for( int i = 0; i < json.getJSONArray("authorities").length(); i++) {
                JSONObject authority = (JSONObject) json.getJSONArray("authorities").get(i);
                regions.add(new Region(authority.getString("LocalAuthorityId"), authority.getString("Name")));
            }
        } catch (JSONException e) {
            LOG.error(e.toString());
            throw new JSONException("Failed to parse API response");
        }
        return regions;
    }

    private List<Rating> getRatingsPercentages(Map ratings, int establishmentsTotal) {
        LOG.info("Converting ratings to a percentage");
        List<Rating> ratingsPercent = new ArrayList<>();
        //loop over the map and then calculate percentage to add to a list
        Iterator it = ratings.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            double percentBase = Double.valueOf( pair.getValue().toString());
            percentBase = percentBase / establishmentsTotal * 100;
            String percent = String.format("%.2f", percentBase) + '%';
            Rating rating = new Rating(pair.getKey().toString(), percent);
            ratingsPercent.add(rating);
            it.remove(); // avoids a ConcurrentModificationException
        }
        return ratingsPercent;
    }

    public List<Rating> getRatings(String establishmentsToParse) throws RatingsException {
        LOG.info("Parsing ratings");
        try {
            ObjectMapper mapper= new ObjectMapper();
            Establishments establishments = mapper.readValue(establishmentsToParse, Establishments.class);

            Map<String, Integer> ratings = getRatingsMap();
            for (Establishment establishment: establishments.establishments) {
                Integer score = ratings.get(establishment.ratingValue) + 1;
                ratings.put(establishment.ratingValue, score);
            }
            return getRatingsPercentages(ratings, establishments.establishments.size());

        } catch (IOException e) {
            LOG.error(e.toString());
            throw new RatingsException("Could not get ratings");
        }
    }

    //could have used api here to get all ratings
    private Map<String, Integer> getRatingsMap() {
        Map<String, Integer> ratings = new HashMap<>();
        ratings.put("Exempt",0);
        ratings.put("AwaitingInspection",0);
        ratings.put("0",0);
        ratings.put("1",0);
        ratings.put("2",0);
        ratings.put("3",0);
        ratings.put("4",0);
        ratings.put("5",0);
        return ratings;
    }
}
