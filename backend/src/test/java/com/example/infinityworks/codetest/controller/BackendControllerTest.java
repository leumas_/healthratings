package com.example.infinityworks.codetest.controller;

import com.example.infinityworks.codetest.SpringBootVuejsApplication;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = SpringBootVuejsApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class BackendControllerTest {

	@LocalServerPort
	private int port;

	@Before
    public void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

	@Test
	public void getRegions() {
		when()
			.get("/api/regions")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.assertThat()
				.body("id[0]",equalTo("197"));
	}

	@Test
    public void addNewUserAndRetrieveItBack() {
		when()
		    .get("/api/ratings/1")
		.then()
            .statusCode(HttpStatus.SC_OK)
            .assertThat()
				.body("starValue[0]",equalTo("0"));
    }

}
