package com.example.infinityworks.codetest.functions;

import com.example.infinityworks.codetest.domain.Rating;
import com.example.infinityworks.codetest.domain.Region;
import com.example.infinityworks.codetest.exceptions.RatingsException;
import org.json.JSONException;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class BackendFunctionsTest {
    private BackendFunctions functions = new BackendFunctions();


    private final String regionsToParse = "{ \"authorities\": [ { \"LocalAuthorityId\": 197, " +
            "\"LocalAuthorityIdCode\": \"760\", \"Name\": \"Aberdeen City\", \"EstablishmentCount\": 1776, " +
            "\"SchemeType\": 2, \"links\": [ { \"rel\": \"self\", \"href\": " +
            "\"http://api.ratings.food.gov.uk/authorities/197\"}]},{\"LocalAuthorityId\": 198," +
            "\"LocalAuthorityIdCode\": \"761\", \"Name\": \"Aberdeenshire\", \"EstablishmentCount\": 2122, " +
            "\"SchemeType\": 2, \"links\": [ { \"rel\": \"self\", \"href\": " +
            "\"http://api.ratings.food.gov.uk/authorities/198\" } ] }]}";

    private final String establishmentsToParse = "{ \"establishments\": [ { \"FHRSID\": 250893, " +
            "\"LocalAuthorityBusinessID\": \"5756\", \"BusinessName\": \"12 Harland Place\", \"BusinessType\": " +
            "\"Restaurant/Cafe/Canteen\", \"BusinessTypeID\": 1, \"AddressLine1\": \"\", \"AddressLine2\": " +
            "\"12 Harland Place\", \"AddressLine3\": \"\", \"AddressLine4\": \"Norton\", \"PostCode\": \"TS20 1AL\"," +
            "\"Phone\": \"\", \"RatingValue\": \"5\", \"RatingKey\": \"fhrs_5_en-gb\", \"RatingDate\": " +
            "\"2017-06-26T00:00:00\", \"LocalAuthorityCode\": \"862\", \"LocalAuthorityName\": \"Stockton On Tees\"," +
            "\"LocalAuthorityWebSite\": \"http://www.stockton.gov.uk\", \"LocalAuthorityEmailAddress\": " +
            "\"environmental.health@stockton.gov.uk\", \"scores\": { \"Hygiene\": 5, \"Structural\": 5, " +
            "\"ConfidenceInManagement\": 5 }, \"SchemeType\": \"FHRS\", \"geocode\": { \"longitude\": \"-1.309704\"," +
            "\"latitude\": \"54.586619\" }, \"RightToReply\": \"\", \"Distance\": null, \"NewRatingPending\": false," +
            "\"meta\": { \"dataSource\": null, \"extractDate\": \"0001-01-01T00:00:00\", \"itemCount\": 0, " +
            "\"returncode\": null, \"totalCount\": 0, \"totalPages\": 0, \"pageSize\": 0, \"pageNumber\": 0 }, " +
            "\"links\": [] }, { \"FHRSID\": 250690, \"LocalAuthorityBusinessID\": \"101261\", \"BusinessName\": " +
            "\"14 Forty\", \"BusinessType\": \"Restaurant/Cafe/Canteen\", \"BusinessTypeID\": 1, \"AddressLine1\": " +
            "\"Marshalls Mono Limited\", \"AddressLine2\": \"Durham Lane\", \"AddressLine3\": \"\", \"AddressLine4\":" +
            "\"Eaglescliffe\", \"PostCode\": \"TS16 0PS\", \"Phone\": \"\", \"RatingValue\": \"3\", \"RatingKey\": " +
            "\"fhrs_5_en-gb\", \"RatingDate\": \"2018-02-20T00:00:00\", \"LocalAuthorityCode\": \"862\", " +
            "\"LocalAuthorityName\": \"Stockton On Tees\", \"LocalAuthorityWebSite\": \"http://www.stockton.gov.uk\"," +
            "\"LocalAuthorityEmailAddress\": \"environmental.health@stockton.gov.uk\", \"scores\": { \"Hygiene\": 0, " +
            "\"Structural\": 0, \"ConfidenceInManagement\": 0 }, \"SchemeType\": \"FHRS\", \"geocode\": { \"longitude" +
            "\": \"-1.361762\", \"latitude\": \"54.53682\" }, \"RightToReply\": \"\", \"Distance\": null, " +
            "\"NewRatingPending\": false, \"meta\": { \"dataSource\": null, \"extractDate\": \"0001-01-01T00:00:00\"," +
            "\"itemCount\": 0, \"returncode\": null, \"totalCount\": 0, \"totalPages\": 0, \"pageSize\": 0, " +
            "\"pageNumber\": 0 }, \"links\": [] } ], \"meta\": { \"dataSource\": \"Lucene\", \"extractDate\": " +
            "\"0001-01-01T00:00:00\", \"itemCount\": 0, \"returncode\": \"OK\", \"totalCount\": 1462, \"totalPages\":" +
            "1, \"pageSize\": 5000, \"pageNumber\": 1 }, \"links\": [] }";

    @Test
    public void parseRegions() {
        List<Region> regions = null;
        try {
            regions = functions.parseRegions(regionsToParse);
        } catch (JSONException e) {
            fail();
        }
        assertTrue(regions.size() == 2);
    }

    @Test
    public void parseRegionsWillThrowExceptionIfJsonIncorrect() {
        try {
            functions.parseRegions(regionsToParse.substring(20));
            fail();
        } catch (JSONException e) {
            assert (e.getMessage().equals("Failed to parse API response"));
        }
    }

    @Test
    public void getRatings() {
        List<Rating> ratings = null;
        try {
            ratings = functions.getRatings(establishmentsToParse);
        } catch (Exception e) {
            fail();
        }
        assertTrue(ratings.size() == 8);
    }

    @Test
    public void getRatingsThrows() {
        try {
            functions.getRatings(establishmentsToParse.substring(50));
            fail();
        } catch (RatingsException e) {
            assert (e.getMessage().equals("Could not get ratings"));
        }
    }
}