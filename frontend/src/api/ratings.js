import {AXIOS} from '../lib/http-common'

export default async (urlString) => {
    const response = await AXIOS.get(urlString);
    return response.data;
}