import {AXIOS} from '../lib/http-common'

export default async () => {
    const response = await AXIOS.get(`/regions`);
    return response.data;
}
