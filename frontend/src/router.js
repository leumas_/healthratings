import Vue from 'vue'
import Router from 'vue-router'
import Ratings from '@/pages/Ratings'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Ratings',
      component: Ratings
    }
  ]
})
