import { shallowMount } from '@vue/test-utils';
import Ratings from '@/components/Ratings'

describe('Ratings.vue', () => {
  it('should render correct message', () => {
    // Given
    const Ratingswrapped = shallowMount(Ratings, {
      // see https://stackoverflow.com/a/37940045/4964553
      stubs: ['router-link', 'router-view']
    });

    // When
    const contentH1 = Ratingswrapped.find('h1');

    // Then
    expect(contentH1.text()).toEqual('This will get the food ratings Please select a region and the ratings will be calculated:');
  }),
  it('should render Get ratings for this region Button', () => {
    const wrapper = shallowMount(Ratings);
    const contentButton = wrapper.find('button');
    expect(contentButton.text()).toEqual('Get ratings for this region');
  })

  //given more time I'd test that upon selecting an element then the regions would get updated and also that ratings uses the 
  //correct select value and gets ratings
})
